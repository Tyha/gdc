(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('htWidgetContentSearch', htWidgetContentSearch);

    /* @ngInject */
    function htWidgetContentSearch() {
        //Usage:
        //<div ht-widget-content-search title="vm.map.title"></div>
        // Creates:
        // <div ht-widget-content-search=""
        //      title="Movie"
        //      allow-collapse="true" </div>
        var directive = {
            scope: {
                'subtitle': '@',
                'allowCollapse': '@'
            },
            templateUrl: 'app/widgets/widget-content-search.html',
            restrict: 'EA'
        };
        return directive;
    }
})();
