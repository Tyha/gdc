(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$q', 'dataservice', 'logger', 'moment'];
    /* @ngInject */
    function DashboardController($q, dataservice, logger, moment) {
        var vm = this;
        vm.search = {
            title: 'Rechercher une personne ou une compétence',
            subtitle: 'Rechercher ici: John Doe, Java'
        };

        vm.messageCount = 0;
        vm.people = [];
        vm.title = 'Dashboard';
        vm.todayDate = moment().format('DD / MM / YYYY');

        activate();

        function activate() {
            var promises = [getMessageCount(), getPeople()];
            return $q.all(promises).then(function() {
                logger.info('Vue Dashboard active');
            });
        }

        // Pas utilisé pour le moment (utile pour les alertes)
        function getMessageCount() {
            return dataservice.getMessageCount().then(function (data) {
                vm.messageCount = data;
                return vm.messageCount;
            });
        }

        function getPeople() {
            return dataservice.getPeople().then(function (data) {
                vm.people = data;
                return vm.people;
            });
        }
    }
})();
